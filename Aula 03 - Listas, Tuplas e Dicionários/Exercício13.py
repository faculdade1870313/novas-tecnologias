sandwich_orders = ["Sanduíche de Frango", "Sanduíche de Peru", "Sanduíche Vegetariano", "Sanduíche de Presunto e Queijo"]
finished_sandwiches = []

while sandwich_orders:
    pedido = sandwich_orders.pop(0)
    print(f"Preparando seu sanduíche de {pedido}...")
    finished_sandwiches.append(pedido)

print("\nSanduíches preparados:")
for sanduiche in finished_sandwiches:
    print(f"- {sanduiche}")
