lista = [12, -2, 4, 8, 29, 45, 78, 36, -17, 2, 12, 8, 3, 3, -52]

# a. imprima o maior elemento
print("Maior elemento:", max(lista))

# b. imprima o menor elemento
print("Menor elemento:", min(lista))

# c. imprima os números pares
print("Números pares:", [num for num in lista if num % 2 == 0])

# d. imprima o número de ocorrências do primeiro elemento da lista
print("Número de ocorrências do primeiro elemento:", lista.count(lista[0]))

# e. imprima a média dos elementos
media = sum(lista) / len(lista)
print("Média dos elementos:", media)

# f. imprima a soma dos elementos de valor negativo
soma_negativos = sum(num for num in lista if num < 0)
print("Soma dos elementos negativos:", soma_negativos)
