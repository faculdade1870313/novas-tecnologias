T = [-10, -8, 0, 1, 2, 5, -2, -4]

# Menor temperatura
print("Menor temperatura:", min(T))

# Maior temperatura
print("Maior temperatura:", max(T))

# Temperatura média
media_temperatura = sum(T) / len(T)
print("Temperatura média:", media_temperatura)
