pets = [
    {'animal': 'cachorro', 'dono': 'João'},
    {'animal': 'gato', 'dono': 'Maria'},
    {'animal': 'passarinho', 'dono': 'Pedro'}
]

for pet in pets:
    print("Animal:", pet['animal'].capitalize())
    print("Dono:", pet['dono'].capitalize())
    print()
