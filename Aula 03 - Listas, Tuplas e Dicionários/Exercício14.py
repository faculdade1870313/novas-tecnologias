grid=[['00','01','02'],['10','11','12'],['20','21','22']]

acertou=False
velha=False
erros=0

while not acertou and not velha:

    simbolo=input("Entre com o símbolo: ")
    linha=int(input("Qual linha? "))
    coluna=int(input("Qual posição? "))

    grid[linha] [coluna] = simbolo

    for i, linha_valor in enumerate(grid):
        for j, coluna_valor in enumerate(grid[i]):
            print(grid[i] [j],end=" ")
        print()

    for i in range(0, len(grid)):
        if grid[i] [0] ==grid [i] [1] and grid [i] [1]==grid [i] [2]:
            acertou=True
            break

    for j in range(0, len(grid[0])):
        if grid[0] [j] ==grid [1] [j] and grid [1] [j]==grid [2] [j]:
            acertou=True
            break

    if grid[0] [0] ==grid [1] [1] and grid [1] [1]==grid [2] [2]:
        acertou=True

    if grid[2] [0] ==grid [1] [1] and grid [1] [1]==grid [0] [2]:
        acertou=True
    
    erros+=1
    velha= erros==9

if acertou==True:
    print("Você ganhou!")
else:
    print("Deu velha!")
