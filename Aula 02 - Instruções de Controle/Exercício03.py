def primo(numero):
    if numero <= 1:
        return False
    for i in range(2, int(numero ** 0.5) + 1):
        if numero % i == 0:
            return False
    return True

def imprimir_primos(n):
    contador = 0
    numero = 2
    while contador < n:
        if primo(numero):
            print(numero, end=" ")
            contador += 1
        numero += 1

n = int(input("Digite um número inteiro positivo: "))

print(f"\nOs {n} primeiros números primos são:")
imprimir_primos(n)
