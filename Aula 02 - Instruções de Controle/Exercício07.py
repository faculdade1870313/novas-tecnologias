num = int(input("Digite o número da conta: "))

d1 = num // 1000
d2 = (num % 1000) // 100
d3 = (num % 100) // 10
d4 = num % 10

digito_verificador = (d1 + d2 + d3 + d4) % 10

print(f"Sua conta é {num:06d}-{digito_verificador}")
