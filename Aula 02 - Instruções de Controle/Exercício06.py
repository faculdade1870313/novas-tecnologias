def fibonacci(n):
    fibonacci_antecessor = 1
    fibonacci_atual = 1

    for _ in range(3, n + 1):
        fibonacci_proximo = fibonacci_atual + fibonacci_antecessor
        fibonacci_antecessor = fibonacci_atual
        fibonacci_atual = fibonacci_proximo

    return fibonacci_atual

n = int(input("Insira o valor de n (n >= 3): "))

if n < 3:
    print("Por favor, insira um número maior ou igual a 3.")
else:
    termo_n = fibonacci(n)
    print(f"O {n}-ésimo termo da série de Fibonacci é: {termo_n}")